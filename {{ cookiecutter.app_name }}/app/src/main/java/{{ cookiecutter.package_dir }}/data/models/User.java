package {{ cookiecutter.package_name }}.data.models;

/**
 * Created by Administrator on 12/22/2017.
 */

public class User {
    private String mName;
    private String mPassword;
    private String mEmail;

    public User(String mName, String mPassword, String mEmail) {
        this.mName = mName;
        this.mPassword = mPassword;
        this.mEmail = mEmail;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = mName;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        this.mPassword = mPassword;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }
}
