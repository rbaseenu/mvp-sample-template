package {{ cookiecutter.package_name }}.data;

/**
 * Created by Administrator on 12/22/2017.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}
