package {{ cookiecutter.package_name }}.features.loginSignUp.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import {{ cookiecutter.package_name }}.R;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    private LoginContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mPresenter = new LoginPresenter(this);
    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {

    }
}
