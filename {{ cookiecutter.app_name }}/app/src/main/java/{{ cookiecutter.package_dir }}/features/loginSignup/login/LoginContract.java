package {{ cookiecutter.package_name }}.features.loginSignUp.login;

import {{ cookiecutter.package_name }}.data.BasePresenter;
import {{ cookiecutter.package_name }}.data.BaseView;

/**
 * Created by Administrator on 12/22/2017.
 */

public class LoginContract {
    interface Presenter extends BasePresenter{

    }
    interface View extends BaseView<Presenter>{

    }
}
