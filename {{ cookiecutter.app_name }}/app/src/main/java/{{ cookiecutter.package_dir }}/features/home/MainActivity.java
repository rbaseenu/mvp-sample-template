package {{ cookiecutter.package_name }}.features.home;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import {{ cookiecutter.package_name }}.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
